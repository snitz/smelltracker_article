clear
load('DataV3_Anonymized.mat')
%%

NegativeIntensityRatings.name='Negative';
PositiveIntensityRatings.name='Positive';
NegativePleasantnessRatings.name='Negative';
PositivePleasantnessRatings.name='Positive';
UnconfirmedIntensityRatings.name = 'Unconfirmed';
UnconfirmedPleasantnessRatings.name= 'Unconfirmed';

for i=1:size(data.odors,1)
    i;
    %prepare the odor var names by removing spaces and special characters
    % dirty way to clean up variable names
    current_odors=data.odors(i,:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    
    current_pleasantness=data.pleasant_mat(i,:);
    current_intensity=data.intensity_mat(i,:);
    for j=1:length(current_odors) %clean up of (,), and spaces
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    % now decide which structure to add the ratings to
    if strcmp(data.covid_test(i),'Tested positive')
        % first update intensity for positive subjects
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(PositiveIntensityRatings);
            if find(strcmp(CELL,aux))
                PositiveIntensityRatings.(aux)=[PositiveIntensityRatings.(aux),current_intensity(j)];
            else % the column must be initialized
                PositiveIntensityRatings.(aux)=current_intensity(j);
            end
        end
        % now update pleasantness for positive subjects
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(PositivePleasantnessRatings);
            if find(strcmp(CELL,aux))
                PositivePleasantnessRatings.(aux)=[PositivePleasantnessRatings.(aux),current_pleasantness(j)];
            else % the column must be initialized
                PositivePleasantnessRatings.(aux)=current_pleasantness(j);
            end
        end
        
    elseif strcmp(data.covid_test(i),'Tested negative')
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(NegativeIntensityRatings);
            if find(strcmp(CELL,aux))
                NegativeIntensityRatings.(aux)=[NegativeIntensityRatings.(aux),current_intensity(j)];
            else % the column must be initialized
                NegativeIntensityRatings.(aux)=current_intensity(j);
            end
        end
        % now update pleasantness for negative structures
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(NegativePleasantnessRatings);
            if find(strcmp(CELL,aux))
                NegativePleasantnessRatings.(aux)=[NegativePleasantnessRatings.(aux),current_pleasantness(j)];
            else % the column must be initialized
                NegativePleasantnessRatings.(aux)=current_pleasantness(j);
            end
        end
    else % update negative structures
        % first intensity for negative structures
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(UnconfirmedIntensityRatings);
            if find(strcmp(CELL,aux))
                UnconfirmedIntensityRatings.(aux)=[UnconfirmedIntensityRatings.(aux),current_intensity(j)];
            else % the column must be initialized
                UnconfirmedIntensityRatings.(aux)=current_intensity(j);
            end
        end
        
        % now update pleasantness for negative structures
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(UnconfirmedPleasantnessRatings);
            if find(strcmp(CELL,aux))
                UnconfirmedPleasantnessRatings.(aux)=[UnconfirmedPleasantnessRatings.(aux),current_pleasantness(j)];
            else % the column must be initialized
                UnconfirmedPleasantnessRatings.(aux)=current_pleasantness(j);
            end
        end
        
    end
end

clear CELL i j ans aux current_intensity current_odors current_pleasantness



PositiveIntensityRatings=rmfield(PositiveIntensityRatings,'name');
names=fieldnames(PositiveIntensityRatings);



%%
% creat a matrix of p values for each subject odor ratings

pMat=zeros(size(data.intensity_mat,1),length(fieldnames(PositiveIntensityRatings))+1);
IntMat=zeros(size(data.intensity_mat,1),length(fieldnames(PositiveIntensityRatings))+1);
PlsMat=IntMat;
missing_odors = {};
for i=1:size(data.intensity_mat,1)
    i;
    %prepare the odor var names by removing spaces and special characters
    % dirty way to clean up variable names
    current_odors=data.odors(i,:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    current_pleasantness=data.pleasant_mat(i,:);
    current_intensity=data.intensity_mat(i,:);
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    current_odors(contains(current_odors, 'Coffeeinstnt')) = {'Coffeeinstant'};
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        CELL=fieldnames(PositiveIntensityRatings);
        index=find(strcmpi(CELL,aux));
        try
            negative_ratings=NegativeIntensityRatings.(aux);
        catch
            missing_odors = [missing_odors, {aux}];
            negative_ratings = NaN;
        end
        pVal=(negative_ratings*1/negative_ratings)*length(find(negative_ratings<current_intensity(j)))/length(negative_ratings);
        pMat(i,index)=pVal;
        IntMat(i,index)=current_intensity(j);
        PlsMat(i,index)=current_pleasantness(j);
    end
    
    pMat(i,end)=-1;
    IntMat(i,end)=-1;
    PlsMat(i,end)=-1;
    if strcmp(data.covid_test(i),'Tested positive') % mark pMat in the last column
        pMat(i,end)=1;
        IntMat(i,end)=1;
        PlsMat(i,end)=1;
    elseif strcmp(data.covid_test(i),'Tested negative') % mark pMat in the last column
        pMat(i,end)=0;
        IntMat(i,end)=0;
        PlsMat(i,end)=0;
    end
    
end

%% find the positive indecies
positive_indecies=find(pMat(:,end)==1);
% testing_indecies=randsample(positive_indecies,100);
% training_indecies=setxor(positive_indecies,testing_indecies);
negative_indecies=find(pMat(:,end)==0);
np_indecies=setxor([1:size(pMat,1)],positive_indecies);
% negative_training=randsample(negative_indecies,length(training_indecies));



%% prepare the ratings matrices


negativeRatings=zeros(length(negative_indecies),length(names));
for i=1:length(negative_indecies)
    i;
    current_odors=data.odors(negative_indecies(i),:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    current_intensity=data.intensity_mat(negative_indecies(i),:);
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        %       CELL=fieldnames(PositiveIntensityRatings);
        index=find(strcmp(names,aux));
        negativeRatings(i,index)=current_intensity(j);
    end
end



positiveRatings=zeros(length(positive_indecies),length(names));
for i=1:length(positive_indecies)
    i
    current_odors=data.odors(positive_indecies(i),:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    current_intensity=data.intensity_mat(positive_indecies(i),:);
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        %       CELL=fieldnames(PositiveIntensityRatings);
        index=find(strcmp(names,aux));
        positiveRatings(i,index)=current_intensity(j);
    end
end

npRatings=zeros(length(np_indecies),length(names));
for i=1:length(np_indecies)
    i
    current_odors=data.odors(np_indecies(i),:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    current_intensity=data.intensity_mat(np_indecies(i),:);
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        %       CELL=fieldnames(PositiveIntensityRatings);
        index=find(strcmp(names,aux));
        npRatings(i,index)=current_intensity(j);
    end
end



%% calculate the area under the curve for a classifier by each odor
single_odor_classifier =zeros(1,length(names));
clear TruePositive FalsePositive
%  for i=1:length(names)
% find the ratings of the training set

for j=1:size(negativeRatings,2)
    %aux2=negativeRatings(:,j);  % revert to this line to compare only C19-
    aux1=positiveRatings(:,j);
    %aux2=negativeRatings(:,j);
    aux2=npRatings(:,j);
    % keep only the none zero
    aux1=aux1(find(aux1));
    aux2=aux2(find(aux2));
%      if length(aux1)>20 & length(aux2)<21
%          j
%      end
    % make sure there are enough positive ratings
    if length(aux1)>20 & length(aux2)>20
        for k=1:100
            % what proportion is true positive
            TruePositive(k)=length(find(aux1<=k))/length(aux1);
            FalsePositive(k)=length(find(aux2<=k))/length(aux2);
        end
        % the area under the curve is
        %     auc=sum(TruePositive);
        auc=trapz(FalsePositive,TruePositive);
        single_odor_classifier(j)=auc;
        posCount(j)=length(aux1);
        negCount(j)=length(aux2);
    end
end
clear aux1 aux2 auc j k i aux
%length(find(single_odor_classifier))
[single_odor_classifier_sorted, SortIdx] = sort(single_odor_classifier, 'descend');
SortIdx(single_odor_classifier_sorted==0) = [];
posCount=posCount(SortIdx);
negCount=negCount(SortIdx);





%%
%% calcualte olive oil ROC based on symptoms
clear OliveOilPos OliveOilNeg
CuminandOliveOil=IntMat(:,[7,10,61]);
CuminPositiveInd=intersect(find(CuminandOliveOil(:,3)==1),find(CuminandOliveOil(:,1)));
CuminNPInd=intersect(find(CuminandOliveOil(:,3)~=1),find(CuminandOliveOil(:,1)));
OliveOilPositiveInd=intersect(find(CuminandOliveOil(:,3)==1),find(CuminandOliveOil(:,2)));
OliveOilNPInd=intersect(find(CuminandOliveOil(:,3)~=1),find(CuminandOliveOil(:,2)));
 

cmap = cbrewer('div', 'RdYlGn', length(SortIdx));
% save('fig5a')
%% set up the plot data for 5a odor based

auxa1=CuminandOliveOil(OliveOilPositiveInd,2);
auxa2=CuminandOliveOil(OliveOilNPInd,2);
labels=[ones(size(auxa1));zeros(size(auxa2))];
scores=[-auxa1;-auxa2];
posclass=1;
[Xa1,Ya1,T,AUCa1,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'TVals','All');
[~,~,~,AUCa1,OPTROCPT,SUBY]= perfcurve(labels,scores,1,'NBoot',1000,'TVals','All','alpha',0.31);




%%
    auxa3=sum(data.symptoms(OliveOilPositiveInd,1:9)');
    auxa4=sum(data.symptoms(OliveOilNPInd,1:9)');


% turn the symptoms into a probability
auxa3=auxa3/9;
auxa4=auxa4/9;
labels=[ones(size(auxa3)),zeros(size(auxa4))];
scores=[auxa3,auxa4];
posclass=1;
[Xa2,Ya2,T,AUCa2,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'TVals','All');
[~,~,~,AUCa2,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'TVals','All','alpha',0.31);









%%%%%% 
% now calculate fig 5b

SymptomaticOliveOilPositiveInd=intersect(intersect(find(CuminandOliveOil(:,3)==1),find(CuminandOliveOil(:,2))),find(sum(data.symptoms(:,1:9)')));
SymptomaticOliveOilNPInd=intersect(intersect(find(CuminandOliveOil(:,3)~=1),find(CuminandOliveOil(:,2))),find(sum(data.symptoms(:,1:9)')));

auxb1=CuminandOliveOil(SymptomaticOliveOilPositiveInd,2);
auxb2=CuminandOliveOil(SymptomaticOliveOilNPInd,2);
labels=[ones(size(auxb1));zeros(size(auxb2))];
scores=[-auxb1;-auxb2];
posclass=1;
[Xb1,Yb1,T,AUCb1,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'TVals','All');
[~,~,~,AUCb1,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'TVals','All','alpha',0.31);




  auxb3=sum(data.symptoms(SymptomaticOliveOilPositiveInd,1:9)');
  auxb4=sum(data.symptoms(SymptomaticOliveOilNPInd,1:9)');

    
% try it with the perfcurve

% turn the symptoms into a probability
auxb3=auxb3/10;
auxb4=auxb4/10;
labels=[ones(size(auxb3)),zeros(size(auxb4))];
scores=[auxb3,auxb4];
posclass=1;
[Xb2,Yb2,T,AUCb2,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'TVals','All');
[~,~,~,AUCb2,OPTROCPT,SUBY]=perfcurve(labels,scores,1,'NBoot',1000,'TVals','All','alpha',0.31);


%% now 5c

for i=1:size(pMat,1)
    nonzeroind=find(pMat(i,1:end-1));
    if nonzeroind
    combinedP(i)=prod(pMat(i,nonzeroind));
    end
end
SymptomCount=sum(data.symptoms(:,1:9)');
aSymptomaticInd=find(SymptomCount==0);


aSymptomaticPosInd=intersect(aSymptomaticInd,positive_indecies);
aSymptomaticNPInd=intersect(aSymptomaticInd,np_indecies);



auxc1=1-combinedP(aSymptomaticPosInd);
auxc2=1-combinedP(aSymptomaticNPInd);
labels=[ones(size(auxc1)),zeros(size(auxc2))];
scores=[auxc1,auxc2];
posclass=1;
[Xc1,Yc1,T,~,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'TVals',[0:0.02:1]);
[~,~,~,AUCc1,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'TVals',[0:0.02:1],'alpha',0.31);




% there are place holders this is a plot of symptom-based
% classifier on people without symptoms so all scores are 0



auxc3=CuminandOliveOil(aSymptomaticPosInd,2);
auxc4=CuminandOliveOil(aSymptomaticNPInd,2);
labels=[ones(size(auxc3));zeros(size(auxc4))];
scores=[-auxc3;-auxc4];
scores=zeros(size(scores));
posclass=1;
[Xc2,Yc2,T,AUCc2,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'TVals','All');
[~,~,~,AUCc2,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'TVals','All','alpha',0.31);


%%

 
%% now only the plots
%% 5a
figure
subplot(1,3,1)

ea1=errorbar(Xa1(:,1),Ya1(:,1),Ya1(:,1)-Ya1(:,2),Ya1(:,3)-Ya1(:,1));
ea1.Color='blue'

hold on
ea2=errorbar(Xa2(:,1),Ya2(:,1),Ya2(:,1)-Ya2(:,2),Ya2(:,3)-Ya2(:,1));
ea2.Color='red'

xlim([-0.02,1.02]); ylim([-0.02,1.02]);
xlabel('False positive %') 
ylabel('True positive %')
title('Symptom based ROC with Confidence Bounds')
legend({['olfactions based Auc=',num2str(AUCa1(1))],['Symptom based Auc=',num2str(AUCa2(1))]},'Location','Best')

hold on


axes('Position',[.2 .28 .1 .1])
% for ii = 1:length(SortIdx)
     hold on
    bar(1,AUCa1(1), 'FaceColor','blue')
    hold on
    bar(2,AUCa2(1), 'FaceColor','red')
    ylim([0.65 0.85])
    ea3 = errorbar([1 2],[AUCa1(1) AUCa2(1)],[AUCa1(2)-AUCa1(1), AUCa2(2)-AUCa2(1)],[AUCa1(3)-AUCa1(1), AUCa2(3)-AUCa2(1)]);    
ea3.Color = [0 0 0];                            
ea3.LineStyle = 'none'; 


%% plot 5b

subplot(1,3,2)

eb1=errorbar(Xb1(:,1),Yb1(:,1),Yb1(:,1)-Yb1(:,2),Yb1(:,3)-Yb1(:,1));
eb1.Color='blue'
hold on
eb2=errorbar(Xb2(:,1),Yb2(:,1),Yb2(:,1)-Yb2(:,2),Yb2(:,3)-Yb2(:,1));
eb2.Color='red'

xlim([-0.02,1.02]); ylim([-0.02,1.02]);
xlabel('False positive %') 
ylabel('True positive %')
title('Olfactory ROC on Symptomatics with Confidence Bounds')
% text(10,10,'StAR AUC p=??')

legend({['Olfactory based Auc=',num2str(AUCb1(1))],['Symptom based Auc=',num2str(AUCb2(1))]},'Location','Best')

axes('Position',[.5 .28 .1 .1])
     hold on
    bar(1,AUCb1(1), 'FaceColor','blue')
    hold on
    bar(2,AUCb2(1), 'FaceColor','red')
    ylim([0.5 0.85])
    eb3 = errorbar([1 2],[AUCb1(1) AUCb2(1)],[AUCb1(2)-AUCb1(1), AUCb2(2)-AUCb2(1)],[AUCb1(3)-AUCb1(1), AUCb2(3)-AUCb2(1)]);    
eb3.Color = [0 0 0];                            
eb3.LineStyle = 'none'; 

%% plot 5c
subplot(1,3,3)

ec1=errorbar(Xc1(:,1),Yc1(:,1),Yc1(:,1)-Yc1(:,2),Yc1(:,3)-Yc1(:,1));
ec1.Color='blue'
hold on

ec2=errorbar(Xc2(:,1),Yc2(:,1),Yc2(:,1)-Yc2(:,2),Yc2(:,3)-Yc2(:,1));
ec2.Color='red'

title('ROC for combined probability on asymptomatics')
xlabel('False positive % ') 
ylabel('True positive %')
legend({['Intensity based Auc=',num2str(AUCc1(1))],['Symptom based Auc=',num2str(AUCc2(1))]},'Location','Best')
% text(0.5,0.5,'StAR AUC p=0.00014')

axes('Position',[.8 .28 .1 .1])
% for ii = 1:length(SortIdx)
    hold on
    bar(1,AUCc1(1), 'FaceColor','blue')
    hold on
    bar(2,AUCc2(1), 'FaceColor','red')
    ylim([0.4 0.9])
    ec3 = errorbar([1 2],[AUCc1(1) AUCc2(1)],[AUCc1(2)-AUCc1(1), AUCc2(2)-AUCc2(1)],[AUCc1(3)-AUCc1(1), AUCc2(3)-AUCc2(1)]);    
ec3.Color = [0 0 0];                            
ec3.LineStyle = 'none'; 