clear
load('DataV3_Anonymized.mat')
%% for each odor make a list of positive ratings and negative ratings
NegativeIntensityRatings.name='Negative';
PositiveIntensityRatings.name='Positive';
NegativePleasantnessRatings.name='Negative';
PositivePleasantnessRatings.name='Positive';
UnconfirmedIntensityRatings.name = 'Unconfirmed';
UnconfirmedPleasantnessRatings.name= 'Unconfirmed';

for i=1:size(data.odors,1)
    i;
    %prepare the odor var names by removing spaces and special characters
    % dirty way to clean up variable names
    current_odors=data.odors(i,:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    
    current_pleasantness=data.pleasant_mat(i,:);
    current_intensity=data.intensity_mat(i,:);
    for j=1:length(current_odors) %clean up of (,), and spaces
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    % now decide which structure to add the ratings to
    if strcmp(data.covid_test(i),'Tested positive')
        % first update intensity for positive subjects
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(PositiveIntensityRatings);
            if find(strcmp(CELL,aux))
                PositiveIntensityRatings.(aux)=[PositiveIntensityRatings.(aux),current_intensity(j)];
            else % the column must be initialized
                PositiveIntensityRatings.(aux)=current_intensity(j);
            end
        end
        % now update pleasantness for positive subjects
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(PositivePleasantnessRatings);
            if find(strcmp(CELL,aux))
                PositivePleasantnessRatings.(aux)=[PositivePleasantnessRatings.(aux),current_pleasantness(j)];
            else % the column must be initialized
                PositivePleasantnessRatings.(aux)=current_pleasantness(j);
            end
        end
        
    elseif strcmp(data.covid_test(i),'Tested negative')
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(NegativeIntensityRatings);
            if find(strcmp(CELL,aux))
                NegativeIntensityRatings.(aux)=[NegativeIntensityRatings.(aux),current_intensity(j)];
            else % the column must be initialized
                NegativeIntensityRatings.(aux)=current_intensity(j);
            end
        end
        % now update pleasantness for negative structures
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(NegativePleasantnessRatings);
            if find(strcmp(CELL,aux))
                NegativePleasantnessRatings.(aux)=[NegativePleasantnessRatings.(aux),current_pleasantness(j)];
            else % the column must be initialized
                NegativePleasantnessRatings.(aux)=current_pleasantness(j);
            end
        end
    else % update negative structures
        % first intensity for negative structures
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(UnconfirmedIntensityRatings);
            if find(strcmp(CELL,aux))
                UnconfirmedIntensityRatings.(aux)=[UnconfirmedIntensityRatings.(aux),current_intensity(j)];
            else % the column must be initialized
                UnconfirmedIntensityRatings.(aux)=current_intensity(j);
            end
        end
        
        % now update pleasantness for negative structures
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(UnconfirmedPleasantnessRatings);
            if find(strcmp(CELL,aux))
                UnconfirmedPleasantnessRatings.(aux)=[UnconfirmedPleasantnessRatings.(aux),current_pleasantness(j)];
            else % the column must be initialized
                UnconfirmedPleasantnessRatings.(aux)=current_pleasantness(j);
            end
        end
        
    end
end

clear CELL i j ans aux current_intensity current_odors current_pleasantness



PositiveIntensityRatings=rmfield(PositiveIntensityRatings,'name');
names=fieldnames(PositiveIntensityRatings);



%%
% creat a matrix of p values for each subject odor ratings

pMat=zeros(size(data.intensity_mat,1),length(fieldnames(PositiveIntensityRatings))+1);
IntMat=zeros(size(data.intensity_mat,1),length(fieldnames(PositiveIntensityRatings))+1);
PlsMat=IntMat;
missing_odors = {};
for i=1:size(data.intensity_mat,1)
    i;
    %prepare the odor var names by removing spaces and special characters
    % dirty way to clean up variable names
    current_odors=data.odors(i,:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    current_pleasantness=data.pleasant_mat(i,:);
    current_intensity=data.intensity_mat(i,:);
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    current_odors(contains(current_odors, 'Coffeeinstnt')) = {'Coffeeinstant'};
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        CELL=fieldnames(PositiveIntensityRatings);
        index=find(strcmpi(CELL,aux));
        try
            negative_ratings=NegativeIntensityRatings.(aux);
        catch
            missing_odors = [missing_odors, {aux}];
            negative_ratings = NaN;
        end
        pVal=(negative_ratings*1/negative_ratings)*length(find(negative_ratings<current_intensity(j)))/length(negative_ratings);
        pMat(i,index)=pVal;
        IntMat(i,index)=current_intensity(j);
        PlsMat(i,index)=current_pleasantness(j);
    end
    
    pMat(i,end)=-1;
    IntMat(i,end)=-1;
    PlsMat(i,end)=-1;
    if strcmp(data.covid_test(i),'Tested positive') % mark pMat in the last column
        pMat(i,end)=1;
        IntMat(i,end)=1;
        PlsMat(i,end)=1;
    elseif strcmp(data.covid_test(i),'Tested negative') % mark pMat in the last column
        pMat(i,end)=0;
        IntMat(i,end)=0;
        PlsMat(i,end)=0;
    end
    
end
%%
%nOdorsVec = [2, 3, 4, 5];
nOdorsVec = 4;
for nOdors = nOdorsVec
    
    RatingCount=sum(IntMat(:,1:55)~=0,1);
    [~,ind]=maxk(RatingCount,nOdors);
    Selected_odors = CELL(ind);
    sprintf('%s ', string(Selected_odors(1:nOdors)))
    ind=sort(ind);
    ratedMostInt=[IntMat(:,ind),IntMat(:,end)];
    ratedMostPls=[PlsMat(:,ind),PlsMat(:,end)];
    
    ratedmostInd=find(sum(ratedMostInt~=0,2)==nOdors);
    ratedTopInt=ratedMostInt(ratedmostInd,:);
    ratedTopPls=ratedMostPls(ratedmostInd,:);
    %find(ratedTopInt(:,end))
    
end


%% now run the analysis on only C19-UD people
clear posClassPosteriors



%% identify the people who tested only once
MultiTakerUID = extractfield(data.multi_subjects,'uid');

[~,MultiTakersInd,ib]=intersect(data.uid,MultiTakerUID);

MultiTakersInd=[]
for i=1:length(MultiTakerUID)
   MultiTakersInd=[MultiTakersInd;find(data.uid==MultiTakerUID(i))];
end

SingleTakersInd=setxor([1:length(data.uid)],MultiTakersInd);

%% create fingerprints
for i=1:size(ratedTopPls,1)
    aux=[ratedTopPls(i,1:end-1)',ratedTopInt(i,1:end-1)'];
    D=squareform(pdist(aux));
    index = find(tril(ones(nOdors,nOdors),-1));
    Fingerprints(i,:)=D(index)';
end

FingerprintWithClasses=[Fingerprints,ratedTopInt(:,end)];
resp=FingerprintWithClasses(:,nOdors+1);

% now mark the fingerprints as being multi or single takers
% these indecies are in the FingerprintWithClasses variable
[~,FPSingleTakersInd,~]=intersect(ratedmostInd,SingleTakersInd);
[~,FPMultiTakersInd,~]=intersect(ratedmostInd,MultiTakersInd);

% setdiff(union(FPMultiTakersInd,FPSingleTakersInd),[1:length(ratedmostInd)])
% max(FPMultiTakersInd)
UDInds=find(FingerprintWithClasses(:,end)==-1);
NegInds=find(FingerprintWithClasses(:,end)==0);
PosInds=find(FingerprintWithClasses(:,end)==1);

PositiveFPSingleTakersInd=intersect(FPSingleTakersInd,PosInds);
NegativeFPSingleTakersInd=intersect(FPSingleTakersInd,NegInds);

%% both testing and training sets are indecies in FingerprintWithClasses
for i=1:500
i
testing_set=[randsample(PositiveFPSingleTakersInd,40);randsample(NegativeFPSingleTakersInd,20)];
training_set=setxor([PosInds;NegInds],testing_set);

% now the classifier can be trained
[trainedClassifier, validationAccuracy] = trainSVMClassifier(FingerprintWithClasses(training_set,:));
[label,score] = trainedClassifier.predictFcn(FingerprintWithClasses(testing_set,1:end-1));

% here the variable score is an ordering of the likelyhood of belonging to
% a class
% as we set up the testing_set the first 40 entries are positive and the
% last 20 are negative

aux01=score(1:40,1);
aux02=score(41:60,1);

n=1;
 for j=min([aux01;aux02]):(max([aux01;aux02])-min([aux01;aux02]))/100:max([aux01;aux02])
        % what proportion is true positive
      
        TruePositive(n)=length(find(aux01<=j))/length(aux01);
        FalsePositive(n)=length(find(aux02<=j))/length(aux02);
        n=n+1;
 end
auc=trapz(FalsePositive,TruePositive);
aucAll(i)=auc;
TPAll(i,:)=TruePositive;
FPAll(i,:)=FalsePositive;
Aux1All(i,:)=aux01;
Aux2All(i,:)=aux02;
end
 
% find the median of the auc 

[~, medInd] = min(abs(aucAll-median(aucAll)))
FalsePositive=FPAll(medInd(1),:);
auc=aucAll(medInd(1));
TruePositive=TPAll(medInd(1),:);
    cmap = cbrewer('div', 'RdYlGn', 10);



FingerprintsSymptoms=sum(data.symptoms(ratedmostInd,1:9)')';



%% now using perfcurve

aux1=Aux1All(medInd(1),:);
aux2=Aux2All(medInd(1),:);

% normalize aux1 and aux2
m1=max([aux1,aux2])


labels=[ones(size(aux1)),zeros(size(aux2))];
scores=[-aux1,-aux2];
posclass=1;

[Xa1,Ya1,T,AUCa1,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'Alpha',0.05,'TVals','All','BootType','norm');
% for the purpose of the auc confidence interval do the bootstrap again
[~,~,~,AUCa1,OPTROCPT,SUBY]= perfcurve(labels,scores,1,'NBoot',1000,'alpha',0.31,'TVals','All','BootType','norm');



%% now make a symptom based classifier on the same people

aux3=FingerprintsSymptoms(PosInds);
aux4=FingerprintsSymptoms(NegInds);

labels=[ones(size(aux3));zeros(size(aux4))];
scores=[aux3;aux4];
posclass=1;

[Xa2,Ya2,T,AUCa2] = perfcurve(labels,scores,1,'NBoot',1000,'Alpha',0.05,'TVals','All','BootType','norm');
% for the purpose of the auc confidence interval do the bootstrap again
[~,~,~,AUCa2,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'Alpha',0.31,'TVals','All','BootType','norm');

%% do the calculations for 6b

FingerprintsSymptoms=sum(data.symptoms(ratedmostInd,1:9)')';
FPsymptomsInSymptomaticFP=FingerprintsSymptoms(find(FingerprintsSymptoms));
FPSymptomaticIndInRatings=ratedmostInd(find(FingerprintsSymptoms));
FPSymptomaticIndInFP=find(FingerprintsSymptoms);



FingerprintWithClasses=[Fingerprints,ratedTopInt(:,end)];
SymptomaticFPWithClasses=FingerprintWithClasses(FPSymptomaticIndInFP,:);


UDInds=find(SymptomaticFPWithClasses(:,end)==-1);
NegInds=find(SymptomaticFPWithClasses(:,end)==0);
PosInds=find(SymptomaticFPWithClasses(:,end)==1);
NPInds=find(SymptomaticFPWithClasses(:,end)~=1);


clear Aux1All Aux2All  aucAll
%% this is 6b first part is the data split selection
%% both testing and training sets are indecies in FingerprintWithClasses
for i=1:500
i
testing_set=[randsample(PosInds,41);randsample(NegInds,13)];
training_set=setxor([PosInds;NegInds],testing_set);

% now the classifier can be trained
[trainedClassifier, validationAccuracy] = trainSVMClassifier(SymptomaticFPWithClasses(training_set,:));
[label,score] = trainedClassifier.predictFcn(SymptomaticFPWithClasses(testing_set,1:end-1));

% here the variable score is an ordering of the likelyhood of belonging to
% a class
% as we set up the testing_set the first 40 entries are positive and the
% last 20 are negative

aux01=score(1:41,1);
aux02=score(42:end,1);

n=1;
 for j=min([aux01;aux02]):(max([aux01;aux02])-min([aux01;aux02]))/100:max([aux01;aux02])
        % what proportion is true positive
      
        TruePositive(n)=length(find(aux01<=j))/length(aux01);
        FalsePositive(n)=length(find(aux02<=j))/length(aux02);
        n=n+1;
 end
auc=trapz(FalsePositive,TruePositive);
aucAll(i)=auc;
% TPAll(i,:)=TruePositive;
% FPAll(i,:)=FalsePositive;
Aux1All(i,:)=aux01;
Aux2All(i,:)=aux02;
end
 

[~, medInd] = min(abs(aucAll-median(aucAll)))


%% now using perfcurve

auxb1=Aux1All(medInd(1),:);
auxb2=Aux2All(medInd(1),:);

% normalize aux1 and aux2
m1=max([auxb1,auxb2])


labels=[ones(size(auxb1)),zeros(size(auxb2))];
scores=[-auxb1,-auxb2];
posclass=1;

[Xb1,Yb1,Tb1,AUCb1,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'Alpha',0.05,'TVals','All','BootType','norm');
% [X,Y,T,AUC,OPTROCPT,SUBY] = perfcurve(labels,scores,1,'NBoot',1000,'XVals',[0:0.02:1]);
% for the purpose of the auc confidence interval do the bootstrap again
[~,~,~,AUCb1,~,~]= perfcurve(labels,scores,1,'NBoot',1000,'alpha',0.31,'TVals','All','BootType','norm');






%% now make a symptom based classifier on the same people

auxb3=FPsymptomsInSymptomaticFP(PosInds);
auxb4=FPsymptomsInSymptomaticFP(NegInds);

labels=[ones(size(auxb3));zeros(size(auxb4))];
scores=[auxb3;auxb4];
posclass=1;

[Xb2,Yb2,Tb2,AUCb2] = perfcurve(labels,scores,1,'NBoot',1000,'Alpha',0.05,'TVals','All','BootType','norm');
% for the purpose of the auc confidence interval do the bootstrap again
[~,~,~,AUCb2,~,~] = perfcurve(labels,scores,1,'NBoot',1000,'Alpha',0.31,'TVals','All','BootType','norm');


%%%%%%%%%%%%%%%%%%

% figure
%% now actually plot 6a
figure
subplot(1,2,1)

e1=errorbar(Xa1(:,1),Ya1(:,1),Ya1(:,1)-Ya1(:,2),Ya1(:,3)-Ya1(:,1));
e1.Color='blue'
ylim([0 1])
 title('ROC for FP-SVM on Vs symptoms')
xlabel('False positive %') 
ylabel('True positive %')
hold on

e2=errorbar(Xa2(:,1),Ya2(:,1),Ya2(:,1)-Ya2(:,2),Ya2(:,3)-Ya2(:,1));

% e2=plot(X,Y)
e2.Color='red'
legend({['Olfactory Auc=',num2str(AUCa1(1))],['Symptoms Auc=',num2str(AUCa2(1))]},'Location','Best')
% text(0.5,0.5,'StAR AUC p= p6a')


axes('Position',[.25 .28 .1 .2])
% for ii = 1:length(SortIdx)
     hold on
    bar(1,AUCa2(1), 'FaceColor','red')
    hold on
    bar(2,AUCa1(1), 'FaceColor','blue')
    ylim([0.4 1])
    er = errorbar([1 2],[AUCa2(1) AUCa1(1)],[AUCa2(2)-AUCa2(1), AUCa1(2)-AUCa1(1)],[AUCa2(3)-AUCa2(1), AUCa1(3)-AUCa1(1)]);    
er.Color = [0 0 0];                            
er.LineStyle = 'none'; 

%% now plot 6b
subplot(1,2,2)
e1=errorbar(Xb1(:,1),Yb1(:,1),Yb1(:,1)-Yb1(:,2),Yb1(:,3)-Yb1(:,1));
e1.Color='blue'
ylim([0 1])
 title('ROC for FP-SVM on symptomatics')
xlabel('False positive %') 
ylabel('True positive %')
hold on



e2=errorbar(Xb2(:,1),Yb2(:,1),Yb2(:,1)-Yb2(:,2),Yb2(:,3)-Yb2(:,1));

% e2=plot(X,Y)
e2.Color='red'
legend({['Olfactory Auc=',num2str(AUCb1(1))],['Symptoms Auc=',num2str(AUCb2(1))]},'Location','Best')
text(0.5,0.5,'StAR AUC p=b2')


axes('Position',[.8 .28 .1 .2])
% for ii = 1:length(SortIdx)
     hold on
    bar(1,AUCb2(1), 'FaceColor','red')
    hold on
    bar(2,AUCb1(1), 'FaceColor','blue')
    ylim([0.4 1])
    er = errorbar([1 2],[AUCb2(1) AUCb1(1)],[AUCb2(2)-AUCb2(1), AUCb1(2)-AUCb1(1)],[AUCb2(3)-AUCb2(1), AUCb1(3)-AUCb1(1)]);    
er.Color = [0 0 0];                            
er.LineStyle = 'none'; 
    
    