clear

load('DataV3_Anonymized.mat')



NegativeIntensityRatings.name='Negative';
PositiveIntensityRatings.name='Positive';
NegativePleasantnessRatings.name='Negative';
PositivePleasantnessRatings.name='Positive';
UnconfirmedIntensityRatings.name = 'Unconfirmed';
UnconfirmedPleasantnessRatings.name= 'Unconfirmed';

for i=1:size(data.odors,1)
    i;
    %prepare the odor var names by removing spaces and special characters
    % dirty way to clean up variable names
    current_odors=data.odors(i,:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    
    current_pleasantness=data.pleasant_mat(i,:);
    current_intensity=data.intensity_mat(i,:);
    for j=1:length(current_odors) %clean up of (,), and spaces
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    % now decide which structure to add the ratings to
    if strcmp(data.covid_test(i),'Tested positive')
        % first update intensity for positive subjects
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(PositiveIntensityRatings);
            if find(strcmp(CELL,aux))
                PositiveIntensityRatings.(aux)=[PositiveIntensityRatings.(aux),current_intensity(j)];
            else % the column must be initialized
                PositiveIntensityRatings.(aux)=current_intensity(j);
            end
        end
        % now update pleasantness for positive subjects
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(PositivePleasantnessRatings);
            if find(strcmp(CELL,aux))
                PositivePleasantnessRatings.(aux)=[PositivePleasantnessRatings.(aux),current_pleasantness(j)];
            else % the column must be initialized
                PositivePleasantnessRatings.(aux)=current_pleasantness(j);
            end
        end
        
    elseif strcmp(data.covid_test(i),'Tested negative')
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(NegativeIntensityRatings);
            if find(strcmp(CELL,aux))
                NegativeIntensityRatings.(aux)=[NegativeIntensityRatings.(aux),current_intensity(j)];
            else % the column must be initialized
                NegativeIntensityRatings.(aux)=current_intensity(j);
            end
        end
        % now update pleasantness for negative structures
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(NegativePleasantnessRatings);
            if find(strcmp(CELL,aux))
                NegativePleasantnessRatings.(aux)=[NegativePleasantnessRatings.(aux),current_pleasantness(j)];
            else % the column must be initialized
                NegativePleasantnessRatings.(aux)=current_pleasantness(j);
            end
        end
    else % update negative structures
        % first intensity for negative structures
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(UnconfirmedIntensityRatings);
            if find(strcmp(CELL,aux))
                UnconfirmedIntensityRatings.(aux)=[UnconfirmedIntensityRatings.(aux),current_intensity(j)];
            else % the column must be initialized
                UnconfirmedIntensityRatings.(aux)=current_intensity(j);
            end
        end
        
        % now update pleasantness for negative structures
        for j=1:length(current_odors)
            aux=current_odors{j};
            % check if the odor is in the structure
            CELL=fieldnames(UnconfirmedPleasantnessRatings);
            if find(strcmp(CELL,aux))
                UnconfirmedPleasantnessRatings.(aux)=[UnconfirmedPleasantnessRatings.(aux),current_pleasantness(j)];
            else % the column must be initialized
                UnconfirmedPleasantnessRatings.(aux)=current_pleasantness(j);
            end
        end
        
    end
end

clear CELL i j ans aux current_intensity current_odors current_pleasantness



PositiveIntensityRatings=rmfield(PositiveIntensityRatings,'name');
names=fieldnames(PositiveIntensityRatings);



%%
% creat a matrix of p values for each subject odor ratings

pMat=zeros(size(data.intensity_mat,1),length(fieldnames(PositiveIntensityRatings))+1);
IntMat=zeros(size(data.intensity_mat,1),length(fieldnames(PositiveIntensityRatings))+1);
PlsMat=IntMat;
missing_odors = {};
for i=1:size(data.intensity_mat,1)
    i;
    %prepare the odor var names by removing spaces and special characters
    % dirty way to clean up variable names
    current_odors=data.odors(i,:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    current_pleasantness=data.pleasant_mat(i,:);
    current_intensity=data.intensity_mat(i,:);
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    current_odors(contains(current_odors, 'Coffeeinstnt')) = {'Coffeeinstant'};
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        CELL=fieldnames(PositiveIntensityRatings);
        index=find(strcmpi(CELL,aux));
        try
            negative_ratings=NegativeIntensityRatings.(aux);
        catch
            missing_odors = [missing_odors, {aux}];
            negative_ratings = NaN;
        end
        pVal=(negative_ratings*1/negative_ratings)*length(find(negative_ratings<current_intensity(j)))/length(negative_ratings);
        pMat(i,index)=pVal;
        IntMat(i,index)=current_intensity(j);
        PlsMat(i,index)=current_pleasantness(j);
    end
    
    pMat(i,end)=-1;
    IntMat(i,end)=-1;
    PlsMat(i,end)=-1;
    if strcmp(data.covid_test(i),'Tested positive') % mark pMat in the last column
        pMat(i,end)=1;
        IntMat(i,end)=1;
        PlsMat(i,end)=1;
    elseif strcmp(data.covid_test(i),'Tested negative') % mark pMat in the last column
        pMat(i,end)=0;
        IntMat(i,end)=0;
        PlsMat(i,end)=0;
    end
    
end

%% find the positive indecies
positive_indecies=find(pMat(:,end)==1);
negative_indecies=find(pMat(:,end)==0);
np_indecies=setxor([1:size(pMat,1)],positive_indecies);

%% prepare the ratings matrices

negativeRatings=zeros(length(negative_indecies),length(names));
for i=1:length(negative_indecies)
    i;
    current_odors=data.odors(negative_indecies(i),:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    current_intensity=data.intensity_mat(negative_indecies(i),:);
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        %       CELL=fieldnames(PositiveIntensityRatings);
        index=find(strcmp(names,aux));
        negativeRatings(i,index)=current_intensity(j);
    end
end



positiveRatings=zeros(length(positive_indecies),length(names));
for i=1:length(positive_indecies)
    i
    current_odors=data.odors(positive_indecies(i),:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    current_intensity=data.intensity_mat(positive_indecies(i),:);
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        %       CELL=fieldnames(PositiveIntensityRatings);
        index=find(strcmp(names,aux));
        positiveRatings(i,index)=current_intensity(j);
    end
end

npRatings=zeros(length(np_indecies),length(names));
for i=1:length(np_indecies)
    i
    current_odors=data.odors(np_indecies(i),:);
    % clean up missign data with a hack
    current_odors(find(strcmp(current_odors,'')))={'dummy'};
    current_intensity=data.intensity_mat(np_indecies(i),:);
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        aux(aux=='(')='';
        aux(aux==')')='';
        aux(aux==' ')='';
        current_odors{j}=aux;
    end
    
    for j=1:length(current_odors)
        aux=current_odors{j};
        %       CELL=fieldnames(PositiveIntensityRatings);
        index=find(strcmp(names,aux));
        npRatings(i,index)=current_intensity(j);
    end
end

%% calculate the area under the curve for a classifier by each odor
single_odor_classifier =zeros(1,length(names));
clear TruePositive FalsePositive

for j=1:size(negativeRatings,2)
    aux2=negativeRatings(:,j);  % revert to this line to compare only C19-
    aux1=positiveRatings(:,j);

    aux1=aux1(find(aux1));
    aux2=aux2(find(aux2));

    if length(aux1)>20 & length(aux2)>20
        for k=1:100
            % what proportion is true positive
            TruePositive(k)=length(find(aux1<=k))/length(aux1);
            FalsePositive(k)=length(find(aux2<=k))/length(aux2);
        end
        % the area under the curve is
        %     auc=sum(TruePositive);
        auc=trapz(FalsePositive,TruePositive);
        single_odor_classifier(j)=auc;
        posCount(j)=length(aux1);
        negCount(j)=length(aux2);
    end
end
clear aux1 aux2 auc j k i aux
%length(find(single_odor_classifier))
[single_odor_classifier_sorted, SortIdx] = sort(single_odor_classifier, 'descend');
SortIdx(single_odor_classifier_sorted==0) = [];
posCount=posCount(SortIdx);
negCount=negCount(SortIdx);


%%
figure
subplot(2,2,1)
hold on
%cmap = flip(colormap(summer(length(SortIdx))));
cmap = cbrewer('div', 'RdYlGn', length(SortIdx));
n=1;
for ii = SortIdx
    %ind=Indexes(i);
    
    aux1=positiveRatings(:,ii);
    aux2=negativeRatings(:,ii);
    % keep only the none zero
    aux1=aux1(find(aux1));
    aux2=aux2(find(aux2));
      
%      aux1=PositiveIntensityRatings.(names{ii});
%      aux2=NegativeIntensityRatings.(names{ii});
    % make sure there are enough positive ratings
    for j=1:100
        % what proportion is true positive
        TruePositive(j)=length(find(aux1<=j))/length(aux1);
        FalsePositive(j)=length(find(aux2<=j))/length(aux2);
    end
    plot(FalsePositive,TruePositive, 'Color', [cmap(n, :) 0.6], 'LineWidth', 1.5)
    inds=find(TruePositive>0.78);
    FPPCR(n)=FalsePositive(inds(1));
    colormap(summer)
    n = n+1;
    box on
end
xlabel('False positive','FontSize', 18)
ylabel('True positive','FontSize', 18)
title('4a ROC for single-odor classifiers C19+ vs C19-','FontSize', 12)
% text(0.025,0.95,'(a)','Units','normalized','FontSize',12)
%[hleg, hobj, hout, mout] =
%legend({names{SortIdx}} ,'FontSize', 16,'Location','bestoutside');
%set(hobj,'linewidth',3);
%legend boxoff
% set(gca, 'FontSize', 16, 'LineWidth', 1)
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.56, 0.76]);

subplot(2,2,2)
%axes('Position',[.7 .68 .20 .20])
for ii = 1:length(SortIdx)
    hold on
    bar(ii-0.5, single_odor_classifier_sorted(ii), 'FaceColor', cmap(ii,:))    
end
% text(1:length(SortIdx),single_odor_classifier_sorted(1:length(SortIdx)),num2str(posCount'),'vert','bottom','horiz','center'); 
% text(1:length(SortIdx),single_odor_classifier_sorted(1:length(SortIdx))+0.06,num2str(negCount'),'vert','bottom','horiz','center'); 
for k=1:length(posCount)
    labels{k}= [num2str(posCount(k)),'/',num2str(negCount(k))];
end
% h=text(1:length(SortIdx),single_odor_classifier_sorted(1:length(SortIdx))+0.14,labels,'vert','bottom','horiz','center');
  h=text(1:length(SortIdx),single_odor_classifier_sorted(1:length(SortIdx))+0.2,labels,'vert','bottom','horiz','center');

set(h,'Rotation',90);

ylabel('AUC')
xticks(0:length(SortIdx)-1)
xticklabels(names(SortIdx))
xtickangle(45)
set(gca, 'FontSize', 10,'LineWidth',1)
title('4b')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%555

%%

%% calculate the area under the curve for a classifier by each odor
single_odor_classifier =zeros(1,length(names));
clear TruePositive FalsePositive
%  for i=1:length(names)
% find the ratings of the training set

for j=1:size(negativeRatings,2)
    %aux2=negativeRatings(:,j);  % revert to this line to compare only C19-
    aux1=positiveRatings(:,j);
    %aux2=negativeRatings(:,j);
    aux2=npRatings(:,j);
    % keep only the none zero
    aux1=aux1(find(aux1));
    aux2=aux2(find(aux2));
%      if length(aux1)>20 & length(aux2)<21
%          j
%      end
    % make sure there are enough positive ratings
    if length(aux1)>20 & length(aux2)>20
        for k=1:100
            % what proportion is true positive
            TruePositive(k)=length(find(aux1<=k))/length(aux1);
            FalsePositive(k)=length(find(aux2<=k))/length(aux2);
        end
        % the area under the curve is
        %     auc=sum(TruePositive);
        auc=trapz(FalsePositive,TruePositive);
        single_odor_classifier(j)=auc;
        posCount(j)=length(aux1);
        negCount(j)=length(aux2);
    end
end
clear aux1 aux2 auc j k i aux
%length(find(single_odor_classifier))
[single_odor_classifier_sorted, SortIdx] = sort(single_odor_classifier, 'descend');
SortIdx(single_odor_classifier_sorted==0) = [];
posCount=posCount(SortIdx);
negCount=negCount(SortIdx);

%%

subplot(2,2,3)
hold on
cmap = cbrewer('div', 'RdYlGn', length(SortIdx));
n=1;
for ii = SortIdx
    %ind=Indexes(i);
    
    aux1=positiveRatings(:,ii);
    aux2=npRatings(:,ii);
    
    aux1=aux1(find(aux1));
    aux2=aux2(find(aux2));
      

    for j=1:101
        % what proportion is true positive
        TruePositive(j)=length(find(aux1<j))/length(aux1);
        FalsePositive(j)=length(find(aux2<j))/length(aux2);
    end
    plot(FalsePositive,TruePositive, 'Color', [cmap(n, :) 0.6], 'LineWidth', 1.5)
%     inds=find(TruePositive>0.78);
%     FPPCR(n)=FalsePositive(inds(1));
     colormap(summer)
    n = n+1;
%     box on
end
xlabel('False positive','FontSize', 18)
ylabel('True positive','FontSize', 18)
title('(4c) ROC for single-odor classifiers C19+ vs C19-UD and C19-','FontSize', 12)


% set(gca, 'FontSize', 12, 'LineWidth', 1)
% set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.56, 0.76]);

% axes('Position',[.54 .28 .35 .35])
subplot(2,2,4)
for ii = 1:length(SortIdx)
    hold on
    bar(ii-0.5, single_odor_classifier_sorted(ii), 'FaceColor', cmap(ii,:))    
end

for k=1:length(posCount)
    labels{k}= [num2str(posCount(k)),'/',num2str(negCount(k))];
end
 h=text(1:length(SortIdx),single_odor_classifier_sorted(1:length(SortIdx))+0.22,labels,'vert','bottom','horiz','center');
 set(h,'Rotation',90);
title('4d')
ylabel('AUC')
xticks(0:length(SortIdx)-1)
xticklabels(names(SortIdx))
xtickangle(45)

set(gca, 'FontSize', 10,'LineWidth',1)

