function [f] = PlotBars(WhattoPlot,WhattoPlot_SE, HowMany, unique_odors, titleSTR, Ourcolor, Where_to_plot)
%PlotBars plots bars of stuff
%   and its errorbars
if nargin < 7 || isempty(Where_to_plot)
    Where_to_plot = 1:size(WhattoPlot,2);
end
DarkGrey = [15 15 15]/255;
unique_odors = unique_odors';
hBar = bar(Where_to_plot, reshape(WhattoPlot', [1, numel(WhattoPlot)]), 0.2, 'EdgeColor', [1 1 1], 'FaceColor', Ourcolor);
%xshift = cell2mat(get(hBar,'xoffset')');
xtickind = Where_to_plot;
if sum(Where_to_plot(:)==2)
    xticks(sort(xtickind, 'ascend'))
    xticklabels(unique_odors)
    xtickangle(45)
end
%unique_odors = unique_odors';

ylabel(titleSTR)
set(gca, 'FontSize', 16)
%title(sprintf('%s of %d most popular odors', titleSTR, HowMany))
multnum = length(Ourcolor)/HowMany;
if size(Ourcolor, 1)>1
    for k = 1:HowMany %should be changed
        hBar(k).CData = Ourcolor(floor(multnum*k),:);
    end
end
box off
hold on
errorbar(xtickind, reshape(WhattoPlot', [1 HowMany]), reshape(WhattoPlot_SE', [1 HowMany]), ...
    'LineStyle', 'none', 'CapSize', 0, 'LineWidth', 2, 'color', DarkGrey)
ylim([0 100])
end

