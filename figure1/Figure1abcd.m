%figure 2 scatter odor ratings
clear
close all
load('DataV3_Anonymized.mat')


LightGreyTransperant = [0.2157 0.2157 0.2157 0.3];

pallette_GP = [[20, 33, 61]/255; % dark Blue
    ([105, 54, 104])/255;      %// dark purple
    ([167, 68, 130])/255;   %// light purple
    ([255, 53, 98])/255];   %// pink
OneBlue = cbrewer('seq', 'Blues', 3);
cmap_seq = cbrewer('seq', 'Reds', 100);
cmap_div = cbrewer('div', 'RdBu', 100);
cmap_qual = cbrewer('qual', 'Set2', 6);
Bluesequence = cbrewer('seq', 'Blues', 9);
OurRed = [237 32 36]/255;
OurBlue = [49 130 190]/255;
OurGrey = [55 55 55]/255;
ShouldISave = 0;


for ii =1:5
    tempOdors = unique(categorical(data.odors(:,ii)));
    tempcounts = countcats(categorical(data.odors(:,ii)),1);
    B(1:length(tempcounts),ii) = countcats(categorical(data.odors(:,ii)),1);
    OdorsCategories(1:length(tempOdors),ii) = tempOdors;
end
[~,I] = sort(B,1,'Descend');
for ii =1:5
    Odors_sorted(:,ii) = OdorsCategories(I(:,ii),ii);
end
%%
sz = 20; capsize = 0; linewidth = 2;
HowMany = 23;
plotX = 3; plotY = 3;
text_ind = [2, 105];
Negatives = 1;
Unknown = 2;
Positives = 3;

% intensity_mat_exc_no = data.symptoms(:, 8);
% intensity_mat_exc_yes = ~data.symptoms(:, 8);
figure

% Unknown = 2;
subplot(plotX,plotY,Unknown)
facealpha = 0.01;
hold on
tested_unknown = ~strcmp(data.covid_test, 'Tested positive') & ~strcmp(data.covid_test, 'Tested negative');
Smell_Condition = tested_unknown;

x = data.pleasant_mat(Smell_Condition, :);
y = data.intensity_mat(Smell_Condition, :);
for ii = 1:5
    scatter(x(:,ii), y(:,ii), sz, 'filled', ...
        'MarkerFaceColor', 'k', 'MarkerFaceAlpha', facealpha)
end
pleasant1 = x;
intensity1 = y;
facealpha = 0.4;

%%%%%%% main mean - unknown %%%%%%%
subplot(plotX,plotY,Unknown), hold on
% scatter(nanmean(x, 'all'), nanmean(y, 'all'), ...
%     100*(nanstd(nanstd(x))^2+nanstd(nanstd(y))^2), '+', 'MarkerEdgeColor', 'k', 'LineWidth', 4);%OneBlue(3,:));
errorbar(nanmean(x, 'all'), nanmean(y, 'all'),nanstd(nanstd(x)), 'o', 'Color', 'k',  'MarkerSize', 0.01, 'CapSize', capsize, 'LineWidth', linewidth)
errorbar(nanmean(x, 'all'), nanmean(y, 'all'), nanstd(nanstd(y)), 'horizontal', 'o', 'Color', 'k',  'MarkerSize', 0.01, 'CapSize', capsize, 'LineWidth', linewidth)
text(text_ind(1), text_ind(2), sprintf('n = %d', sum(Smell_Condition)), 'FontSize', 10)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tested_positive = strcmp(data.covid_test, 'Tested positive');
tested_negative = strcmp(data.covid_test, 'Tested negative');

%for negative: Negatives = 1;
Smell_Condition = tested_negative;
%%%%%%% main plot - negative %%%%%%%
subplot(plotX,plotY,Negatives), hold on
x = data.pleasant_mat(Smell_Condition, :);
y = data.intensity_mat(Smell_Condition, :);
for ii = 1:5
    scatter(x(:, ii), y(:, ii), sz, 'filled', ...
        'MarkerFaceColor', OneBlue(3,:), 'MarkerFaceAlpha', facealpha)
    % stds_Neg(n,1) = nanstd(data.pleasant_mat(odor_ind));
    % stds_Neg(n,2) = nanstd(data.intensity_mat(odor_ind));
end
text(text_ind(1), text_ind(2), sprintf('n = %d', sum(Smell_Condition)), 'FontSize', 10)
pleasant2 = x;
intensity2 = y;

%%%%%%% main mean - negative %%%%%%%
subplot(plotX,plotY,Negatives), hold on
errorbar(nanmean(x, 'all'), nanmean(y, 'all'),nanstd(nanstd(x)), 'o', 'Color', 'k',  'MarkerSize', 0.01, 'CapSize', capsize, 'LineWidth', linewidth)
errorbar(nanmean(x, 'all'), nanmean(y, 'all'), nanstd(nanstd(y)), 'horizontal', 'o', 'Color', 'k',  'MarkerSize', 0.01, 'CapSize', capsize, 'LineWidth', linewidth)

%%%%%%% main plot - positive %%%%%%% Positives = 3;
Smell_Condition = tested_positive;

subplot(plotX,plotY,Positives), hold on
x = data.pleasant_mat(Smell_Condition, :);
y = data.intensity_mat(Smell_Condition, :);
scatter(x(:,ii), y(:,ii), sz, 'filled', ...
    'MarkerFaceColor', 'r', 'MarkerFaceAlpha', facealpha)
text(text_ind(1),text_ind(2), sprintf('n = %d', sum(Smell_Condition)), 'FontSize', 10)
pleasant3 = x;
intensity3 = y;

subplot(plotX,plotY,Positives), hold on
errorbar(nanmean(x, 'all'), nanmean(y, 'all'),nanstd(nanstd(x)), 'o', 'Color', 'k',  'MarkerSize', 0.01, 'CapSize', capsize, 'LineWidth', linewidth)
errorbar(nanmean(x, 'all'), nanmean(y, 'all'), nanstd(nanstd(y)), 'horizontal', 'o', 'Color', 'k',  'MarkerSize', 0.01, 'CapSize', capsize, 'LineWidth', linewidth)

Smell_Condition = tested_positive;
n = 1;

subplot(plotX,plotY,Unknown)
%title('Unknown')
xlabel('Pleasantness')
ylabel('Intensity')
set(gca, 'FontSize', 14, 'linewidth',2)
box on

subplot(plotX,plotY,Negatives)
%title('Negatively diagnosed')
xlabel('Pleasantness')
ylabel('Intensity')
set(gca, 'FontSize', 14, 'linewidth',2)
box on

subplot(plotX,plotY,Positives)
%title('Positively diagnosed')
xlabel('Pleasantness')
ylabel('Intensity')
set(gca, 'FontSize', 14, 'linewidth',2)
box on

box on
set(gca, 'FontSize', 14, 'linewidth',2)
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0, 0.56, 0.96]);

if ShouldISave
    saveas(gcf, [FigDir 'ScatterOdorRatings_centroids.jpg'])
end


%%
% new_de
%%
colors.reds = OurRed;
colors.blues = OurBlue;
colors.greys = OurGrey;
HowMany = 23;
HowMany_vec = 1:4:1000;

load('fullT_mat.mat')

intensity_odors_ind = full_T{:,1};
pleasantness_odors_ind = full_T{:,2};



%%
%%%%%%%%%%%%%%%%%%%%%%% d %%%%%%%%%%%%%%%%%%%%%%%
%plot bars for INTENSITY
subplot(plotX,plotY,4:6), hold on
%  map_odor_names = createOdorMapping(Odors_sorted,intensity_odors_ind);
% ugly hack
% map_odor_names={'Oregano'  'Mustard (Dijon)' 'Ketchup' 'Black pepper (ground)' 'Your Shampoo' 'Your perfume'  'Cinnamon'  'Garlic (freshly chopped)' 'Your toothpaste' 'Nutella' 'Soy sauce' 'Vanilla extract' 'Coffee (instant)'  'Your hand soap'  'Vinegar (white)' 'Coconut oil' 'Peanut butter' 'Honey' 'Coffee (ground)' 'Strawberry jam' 'Cumin' 'Olive oil' 'Vinegar (apple)'  'Your laundry detergent' 'Onion (freshly chopped)'};
map_odor_names={ 'Basil' 'Olive oil' 'Vinegar (apple)'  'Cumin' 'Peanut butter' 'Coffee (ground)' 'Honey' 'Vinegar (white)' 'Your hand soap' 'Coconut oil' 'Coffee (instant)'  'Vanilla extract' 'Soy sauce' 'Nutella' 'Your toothpaste' 'Garlic (freshly chopped)' 'Cinnamon'    'Your perfume'    'Your Shampoo'    'Ketchup'    'Black pepper (ground)'  'Mustard (Dijon)'    'Oregano'};

map_odor_names=map_odor_names(1:23)';
clear map_odor_names_NoSpaces
% remove spaces
    for j=1:length(map_odor_names)
        aux=map_odor_names(j);
%         aux(aux == ' ') = []
       aux=strrep(aux,')','');
       aux=strrep(aux,'(','');
       map_odor_names_NoSpaces(j)=strrep(aux,' ','');
    end
map_odor_names_NoSpaces=map_odor_names_NoSpaces';

%% sort the odors by the effect size as given in full_T intensity
for i=1:length(map_odor_names)
%     aux=map_odor_names_NoSpaces{i}
    effectInds(i)=find(strcmp(map_odor_names_NoSpaces{i},intensity_odors_ind));
end    

[~,ind]=sort(effectInds);
map_odor_names=map_odor_names(ind); % the reordering might have been unneccesary

%% end ugly hack

PorI = "I";
plotlabel = 'Intensity';
for Condition = 1:3
    switch Condition
        case 1
            Smell_Condition = tested_positive;
            cmap_seq = colors.reds;
            Where_to_plot = HowMany_vec(1:HowMany);
        case 2
            Smell_Condition = tested_negative;
            cmap_seq = colors.blues;
            Where_to_plot = HowMany_vec(1:HowMany)+1;
        case 3
            Smell_Condition = tested_unknown;
            cmap_seq = colors.greys;
            Where_to_plot = HowMany_vec(1:HowMany)+2;
    end
    
    [odor_mean_intensity,odor_mean_intensity_se] = ...
        odor_ratings_byCondition(map_odor_names,data, Smell_Condition, HowMany, PorI);
    
    odor_ratings(Condition).(plotlabel) = odor_mean_intensity;
    odor_ratings(Condition).([plotlabel '_SE']) = odor_mean_intensity_se;
    
    PlotBars1(odor_ratings(Condition).(plotlabel),... %WhattoPlot
        odor_ratings(Condition).([plotlabel '_SE']), ... %WhattoPlot SE
        HowMany, map_odor_names, plotlabel, cmap_seq, Where_to_plot);
end
%%
%%%%%%%%%%%%%%%%%%%%%%% e %%%%%%%%%%%%%%%%%%%%%%%
%  map_odor_names = createOdorMapping(Odors_sorted,pleasantness_odors_ind);

%% ungly hack 
% map_odor_names={'Oregano'  'Mustard (Dijon)' 'Ketchup' 'Black pepper (ground)' 'Your Shampoo' 'Your perfume'  'Cinnamon'  'Garlic (freshly chopped)' 'Your toothpaste' 'Nutella' 'Soy sauce' 'Vanilla extract' 'Coffee (instant)'  'Your hand soap'  'Vinegar (white)' 'Coconut oil' 'Peanut butter' 'Honey' 'Coffee (ground)' 'Strawberry jam' 'Cumin' 'Olive oil' 'Vinegar (apple)'  'Your laundry detergent' 'Onion (freshly chopped)'};
map_odor_names={ 'Basil' 'Olive oil' 'Vinegar (apple)'  'Cumin' 'Peanut butter' 'Coffee (ground)' 'Honey' 'Vinegar (white)' 'Your hand soap' 'Coconut oil' 'Coffee (instant)'  'Vanilla extract' 'Soy sauce' 'Nutella' 'Your toothpaste' 'Garlic (freshly chopped)' 'Cinnamon'    'Your perfume'    'Your Shampoo'    'Ketchup'    'Black pepper (ground)'  'Mustard (Dijon)'    'Oregano'};
map_odor_names=map_odor_names(1:23)';
clear map_odor_names_NoSpaces
% remove spaces
    for j=1:length(map_odor_names)
        aux=map_odor_names(j);
%         aux(aux == ' ') = []
       aux=strrep(aux,')','');
       aux=strrep(aux,'(','');
       map_odor_names_NoSpaces(j)=strrep(aux,' ','');
    end
map_odor_names_NoSpaces=map_odor_names_NoSpaces';

%% sort the odors by the effect size as given in full_T pleasantness
for i=1:length(map_odor_names)
%     aux=map_odor_names_NoSpaces{i}
    effectInds(i)=find(strcmp(map_odor_names_NoSpaces{i},pleasantness_odors_ind));
end    

[~,ind]=sort(effectInds);
map_odor_names=map_odor_names(ind); % the reordering is necessary here by pleasantness effect size

%% end ungly hack



subplot(plotX,plotY,7:9), hold on

PorI = "p";
plotlabel = 'Pleasantness';

for Condition = 1:3
    switch Condition
        case 1
            Smell_Condition = tested_positive;
            cmap_seq = colors.reds;
            Where_to_plot = HowMany_vec(1:HowMany);
        case 2
            Smell_Condition = tested_negative;
            cmap_seq = colors.blues;
            Where_to_plot = HowMany_vec(1:HowMany)+1;
        case 3
            Smell_Condition = tested_unknown;
            cmap_seq = colors.greys;
            Where_to_plot = HowMany_vec(1:HowMany)+2;
    end
    
    [odor_mean_intensity,odor_mean_intensity_se] = ...
        odor_ratings_byCondition(map_odor_names,data, Smell_Condition, HowMany, PorI);
    
    odor_ratings(Condition).(plotlabel) = odor_mean_intensity;
    odor_ratings(Condition).([plotlabel '_SE']) = odor_mean_intensity_se;
    
    PlotBars1(odor_ratings(Condition).(plotlabel),... %WhattoPlot
        odor_ratings(Condition).([plotlabel '_SE']), ... %WhattoPlot SE
        HowMany, map_odor_names, plotlabel, cmap_seq, Where_to_plot);
end
