function [odor_mean_intensity,odor_mean_intensity_se] = odor_ratings_byCondition(unique_odors,data, Smell_Condition, HowMany, PorI)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

for Qloop = 1:size(unique_odors,2)
    for ii = 1:size(unique_odors,1)
        odor = unique_odors(ii,Qloop);
        %fprintf('Chosen odor is:\n <<< %s >>>\n', string(odor))
        odor_ind = strcmp(data.odors, odor) & Smell_Condition;
        %         odor_mean_pleasantness(ii, Qloop) = mean(data.pleasant_mat(odor_ind));
        %         odor_mean_pleasantness_se(ii, Qloop) = std(data.pleasant_mat(odor_ind))/(sqrt(sum(odor_ind, 'all'))-1);
        if PorI=="I"
            odor_mean_intensity(ii, Qloop) = mean(data.intensity_mat(odor_ind));
            odor_mean_intensity_se(ii, Qloop) = std(data.intensity_mat(odor_ind))/(sqrt(sum(odor_ind, 'all'))-1);
        elseif PorI=="p"
            odor_mean_intensity(ii, Qloop) = mean(data.pleasant_mat(odor_ind));
            odor_mean_intensity_se(ii, Qloop) = std(data.pleasant_mat(odor_ind))/(sqrt(sum(odor_ind, 'all'))-1);
        end
    end
end
end

