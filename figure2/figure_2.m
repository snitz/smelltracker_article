%% This scripts generates figure 2 of the paper
% addpath(genpath('./'))
load('DataV3_Anonymized.mat')

covid_global_comulative = readtable('time_series_covid19_confirmed_global.csv','ReadVariableNames',true);
% downloaded from:
% https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv

blue_color = [46 131 192] ./ 255;
grey_color = [70, 70, 70] ./ 255;
%%
unique_countries = unique(data.country);
u_vals = cellfun(@(x)(sum(strcmp(data.country, x))), unique_countries);
[u_vals_sorted, idxs] = sort(u_vals, 'descend');

for ii = 1:12
    fprintf('Country: %s, rank: %d, n: %d\n', unique_countries{idxs(ii)}, ii, u_vals_sorted(ii));
end
number_of_countries = 8;
%% Choose top countries
TopCountries = unique_countries(idxs(1:(number_of_countries+1)));
TopCountries(strcmp(TopCountries, 'India')) = [];

%% Create the Date vector
date_start = datetime(2020, 03, 15);
newStr = split(strrep(covid_global_comulative.Properties.VariableNames{end}, 'x', ''), '_');
date_end = datetime(str2num(newStr{3})+2000, str2num(newStr{1}), ...
    str2num(newStr{2})); %change to be according to the length of the vector
date_range = date_start:date_end;
covid_global_new_cases = covid_global_comulative;

date_start_idx = find(strcmp(covid_global_comulative.Properties.VariableNames, sprintf('x%d_%d_%d', date_start.Month, date_start.Day, date_start.Year-2000)));
covid_global_new_cases{:, (date_start_idx+1):end} = covid_global_new_cases{:, (date_start_idx+1):end}-covid_global_new_cases{:, date_start_idx:(end-1)};

%% Calculates daily intensity and cases values. Additionaly it finds maximal correlation.
movmean_time = 7;
max_lag = 14;

covid_global_data_all = covid_global_new_cases{:,date_start_idx:end};

r = []; max_r_lag = [];

for ii = 1:length(TopCountries)
    country_of_choice = string(TopCountries{ii});
    
    which_country = covid_global_comulative{:, 'Country_Region'}==country_of_choice;
    which_country_data_idx = string(data.country)==country_of_choice;
    
    %find Smelltracker times and intensity and inverse
    TT_d = moving_avg_daily(data.time(which_country_data_idx), ...
        data.mean_intensity(which_country_data_idx));
    TT_d{:,'intensity_m_inverse'} = 100-TT_d{:,'intensity_m'};
    
    %handle names discrepency
    if country_of_choice == 'United States'
        country_of_choice = "US";
        which_country = covid_global_comulative{:, 'Country_Region'}==country_of_choice;
        country_of_choice = "USA";
    end
    if country_of_choice == 'United Kingdom'
        country_of_choice = "UK";
    end
    global_country_cases_mean = movmean(nansum(covid_global_data_all(which_country, :), 1), ...
        movmean_time);
    
    % align dates, cut covid_global_data according to starting date
    first_smelltracker_time = find(date_range == TT_d.time(1)); %to apply on covid_global_data
    last_smelltracker_time = find(date_range == TT_d.time(end)); %to apply on covid_global_data
    if isempty(last_smelltracker_time)
        last_smelltracker_time = length(date_range);
        last_intensity_idx = find(TT_d.time == date_range(last_smelltracker_time));
        TT_d = TT_d(1:last_intensity_idx, :);
    end
    
    % First_time is the strating date of the smelltracker data for the
    % country
    global_country_cases_mean_dates = global_country_cases_mean(first_smelltracker_time:last_smelltracker_time);
    dates_cases = date_range(first_smelltracker_time:last_smelltracker_time);
    assert(all(dates_cases == TT_d.time'));
    
    [corrs,xcorr_lag] = xcorr(zscore(global_country_cases_mean_dates)', ...
        zscore(TT_d.intensity_m_inverse), max_lag, ...
        'coeff');
    
    r(:, ii) = corrs;
    [max_rLag(ii,1), max_rLag(ii,2)] = max(r(:,ii));
    country_lag = xcorr_lag(max_rLag(ii, 2)); %find lag from center
    
    find_first_intensity = max(-country_lag+1, 1);
    find_last_intensity = min(height(TT_d)-country_lag, height(TT_d));
    
    intensity_data_shifted = TT_d.intensity_m_inverse(find_first_intensity:find_last_intensity);
    if country_lag > 0
        global_country_cases_mean_to_use_time = global_country_cases_mean_dates(country_lag+1:end);
    else
        global_country_cases_mean_to_use_time = global_country_cases_mean_dates(1:(end+country_lag));
    end
    
    [r_val_max, pval(ii)] = ...
        corr(global_country_cases_mean_to_use_time', ...
        intensity_data_shifted);
    
    find_first_intensity = max(-country_lag+1, 1);
    find_last_intensity = min(height(TT_d)-country_lag, height(TT_d));
    shifted_signal = TT_d.intensity_m_inverse(find_first_intensity:find_last_intensity);
    
    %save needed variables
    fig_variables(ii).country = country_of_choice;
    fig_variables(ii).time_global = date_range;
    fig_variables(ii).country_cases = global_country_cases_mean;
    fig_variables(ii).time_intensity = TT_d.time;
    fig_variables(ii).country_intensity = TT_d.intensity_m_inverse;
    fig_variables(ii).shifted_signal = TT_d.intensity_m_inverse(find_first_intensity:find_last_intensity);
    fig_variables(ii).country_lag = country_lag;
    if country_lag < 1
        fig_variables(ii).time_shifted = TT_d.time(1:length(shifted_signal));
    else
        fig_variables(ii).time_shifted = TT_d.time(country_lag+(1:length(shifted_signal)));
    end
    fig_variables(ii).r_val_max = r_val_max;
    fig_variables(ii).p_val_max = pval(ii);
end

%% Plot sorted subplots
f = figure(1);
set(gcf,'DefaultAxesFontSize',7);
set(gcf,'DefaultAxesFontName','helvetica');
set(gcf,'PaperUnits','centimeters','PaperPosition',[0, 0 18, 24]);
set(gcf,'PaperOrientation','portrait');
set(gcf,'Units','centimeters','Position',get(gcf,'paperPosition')+[1 1 0 0]); % position on screen...
set(gcf, 'Renderer', 'painters');

[r_sorted, sorted_idx] = sort([fig_variables.r_val_max], 'descend');
for ii = 1:8
    subplot_num = sorted_idx(ii);
    subplot(4, 2, ii)
    %retrieve plot variables
    F = fig_variables(subplot_num);
    
    % plot cases
    yyaxis left
    plot(F.time_global, F.country_cases, 'color', grey_color, 'linewidth', .75)
    
    ax = gca;
    ax.XLim = [datetime(2020, 03, 15), datetime(2020, 09, 30)];
    ax.XTick = arrayfun(@(x)(datetime(2020, x, 15)), 3:2:9);
    ax.XTickLabel = string(datestr(ax.XTick, 2));
    ax.XTickLabelRotation = 0;
    ax.FontSize = 7;
    ax.YTickLabel = string(ax.YTick);
    ylab_l{ii} = ylabel('Daily confirmed cases');
    hold on
    
    %plot intensity
    yyaxis right
    hold on;
    ax.YColor = blue_color;
    plot(F.time_intensity, F.country_intensity, ...
        'linewidth',  .75, 'Color', blue_color);
    ylab_r = ylabel('Mean intensity (inverted)');
    
    xlabel('Date')
    
    %plot shifted signal
    plot(F.time_shifted,...
        F.shifted_signal, '--','linewidth', .75, 'Color', blue_color.^2)
    
    title(F.country)
    
    text(date_range(150), 96, sprintf('Lag=%d',F.country_lag), 'fontsize', 8)
    
    p = F.p_val_max;
    base = power(10, -floor(log10(p)))*p;
    pow = floor(log10(p));
    if p < 0.005
        text(date_range(113), 5.3, sprintf('r=%.2f, p=%1.1E',F.r_val_max, p), 'fontsize', 8)
    else
        text(date_range(125), 4, sprintf('r=%.2f, p=%.2f', F.r_val_max, p), 'fontsize', 8)
    end
    ylim([0, 100]);
    box off;
    %legend({'Cases', 'Intensity', 'Shifted Intensity'})
end
%%
figure_name = 'Fig3_daily_InverseIntensity_vectors';

print(figure_name, '-dpdf', '-r300')
print(figure_name, '-dpng', '-r300')
