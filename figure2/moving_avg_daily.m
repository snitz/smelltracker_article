function [T_daily] = moving_avg_daily(time_vec, y1)
% Script moving_avg_daily - computes the daily values of a non-homogenously
% sampled time series
% USAGE example:
% [TT_d] = moving_avg_daily(timeseries, intensity_vec});
% 

%take care that there are no duplicate (by the minute) entries
d = diff(time_vec);
for date_idx = 2:length(time_vec)
    while d(date_idx-1) < minutes(1)
        time_vec(date_idx) = time_vec(date_idx) + minutes(1);
        d = diff(time_vec);
    end
end
TimeWindow = 24*7;
mm = movmean(mean(y1, 2), minutes(60*TimeWindow), 'SamplePoints', time_vec);
ms = movstd(mean(y1, 2), minutes(60*TimeWindow), 'SamplePoints', time_vec);
mn = movsum(ones(size(y1, 1),1), minutes(60*TimeWindow), 'SamplePoints', time_vec);

T = table();
T.time = time_vec;
T.intensity = mean(y1, 2);
T.intensity_m = mm;
T.intensity_std = ms;
T.n = mn;

TT = table2timetable( T, 'rowTimes','time' );
dm = retime( TT, 'daily','count' );

T_daily = retime(TT, 'daily','pchip') ;
T_daily = T_daily(2:end-1, :);
T_daily.count = dm.intensity(2:end);
